<div class="nk-content nk-content-fluid">
    <div class="container-xl wide-xl">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Welcome to Osun State University Task Management System</h3>
                            <div class="nk-block-des text-soft">
                                <p style="font-size: 20px;">Supervisor  <b style="font-size: 20px;"></b></p>
                            </div>
                        </div><!-- .nk-block-head-content -->
                        <div class="nk-block-head-content">
                            <div class="toggle-wrap nk-block-tools-toggle">
                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-more-v"></em></a>
                                <div class="toggle-expand-content" data-content="pageMenu">
                                    <ul class="nk-block-tools g-3">
                                        @role('student')
                                        <li>
                                            <div class="drodown">

                                                <a href="#" class="dropdown-toggle btn btn-white btn-dim btn-outline-light" data-toggle="modal" data-target="#addAdmin"><em class="d-none d-sm-inline icon ni ni-calender-date"></em><span><span class="d-none d-md-inline"></span> Upload Document File</span><em class="dd-indc icon ni ni-chevron-right"></em></a>
{{--                                                <div class="dropdown-menu dropdown-menu-right">--}}
{{--                                                    <ul class="link-list-opt no-bdr">--}}
{{--                                                        <li><a href="#"><span>Last 30 Days</span></a></li>--}}
{{--                                                        <li><a href="#"><span>Last 6 Months</span></a></li>--}}
{{--                                                        <li><a href="#"><span>Last 1 Years</span></a></li>--}}
{{--                                                    </ul>--}}
{{--                                                </div>--}}
                                            </div>
                                        </li>
                                        @endrole
{{--                                        <li class="nk-block-tools-opt"><a href="{{route('show.student')}}" class="btn btn-primary"><em class="icon ni ni-reports"></em><span>Create Student</span></a></li>--}}
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div><!-- .nk-block-head -->

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                @if (session('danger'))
                    <div class="alert alert-danger">
                        {{ session('danger') }}
                    </div>
                @endif
                <div class="nk-block">
                    <div class="row g-gs">
                        <div class="col-xxl-6">
                            <div class="row g-gs">
                                <div class="col-lg-6 col-xxl-12">
                                    <div class="card card-bordered">
                                        <div class="card-inner">
                                            <div class="card-title-group align-start mb-2">
                                                <div class="card-title">
                                                    <h6 class="title">Student</h6>
                                                    <p>Project Topic You Are Working On</p>
                                                </div>
                                                <div class="card-tools">
                                                    <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="Revenue from subscription"></em>
                                                </div>
                                            </div>


                                            <div class="align-end gy-3 gx-5 flex-wrap flex-md-nowrap flex-lg-wrap flex-xxl-nowrap">
                                                <div class="nk-sale-data-group flex-md-nowrap g-4">
                                                    <div class="nk-sale-data">

                                                      @foreach($collection as $con)

                                                          {{$con->project_name}}
                                                        @endforeach

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div><!-- .col -->


                                <div class="col-lg-6 col-xxl-12">
                                    <div class="row g-gs">
                                        <div class="col-sm-6 col-lg-12 col-xxl-6">
                                            <div class="card card-bordered">
                                                <div class="card-inner">
                                                    <div class="card-title-group align-start mb-2">
                                                        <div class="card-title">
                                                            <h6 class="title">Student</h6>
                                                        </div>
                                                        <div class="card-tools">
                                                            <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="Total active subscription"></em>
                                                        </div>
                                                    </div>
                                                    <div class="align-end flex-sm-wrap g-4 flex-md-nowrap">
                                                        <div class="nk-sale-data">
                                                            <span class="amount">{{$studentCount}}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- .card -->
                                        </div><!-- .col -->

                                        </div><!-- .row -->
                                </div><!-- .col -->
                            </div><!-- .row -->
                        </div><!-- .col -->

                        <div class="col-xxl-8">
                            <div class="card card-bordered card-full">
                                <div class="card-inner">
                                    <div class="card-title-group">
                                        <div class="card-title">
                                            <h6 class="title"><span class="mr-2">Latest Task</span></h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-inner p-0 border-top">
                                    <div class="nk-tb-list nk-tb-orders">
                                        <div class="nk-tb-item nk-tb-head">
                                            <div class="nk-tb-col"><span>S/N</span></div>
                                            <div class="nk-tb-col tb-col-sm"><span>Task Description From Supervisor</span></div>
                                            <div class="nk-tb-col tb-col-md"><span>Student</span></div>
                                            <div class="nk-tb-col tb-col-lg"><span>Created At</span></div>

                                        </div>
                                        @foreach($task as $count=>$tasks)
                                            <div class="nk-tb-item">
                                                <div class="nk-tb-col">
                                                    <span class="tb-lead"><a href="#">{{$count+1}}</a></span>
                                                </div>
                                                <div class="nk-tb-col tb-col-lg">
                                                    <span class="tb-sub text-primary">{{$tasks->task_description}}</span>
                                                </div>

                                                <div class="nk-tb-col tb-col-lg">
                                                    <span class="tb-sub text-primary">{{App\User::find($tasks->student_id)->name ?? ""}}</span>
                                                </div>
                                                <div class="nk-tb-col">
                                                    <span class="tb-sub tb-amount"><span>{{db_to_human_time($tasks->created_at)}}</span></span>
                                                </div>

                                            </div>

                                        @endforeach


                                    </div>


                                </div>
                            </div>

                        </div><!-- .card -->
                    </div><!-- .col -->

                </div><!-- .row -->
            </div><!-- .nk-block -->
        </div>
    </div>
</div>
</div>
<!-- content @e -->



<!-- Modal Form -->
<div class="modal fade" tabindex="-1" id="addAdmin">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Upload Your Project to your Assigned Supervisor</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <em class="icon ni ni-cross"></em>
                </a>
            </div>
            <div class="modal-body">
                <form action="{{route('create.upload')}}" method="POST" class="form-validate form is-alter" id="form" enctype="multipart/form-data">

                    <div class="form-group">
                        <label class="form-label" for="default-06">Default File Upload</label>
                        <div class="form-control-wrap">
                            <div class="custom-file">
                                <input type="file" multiple class="custom-file-input" name="file" id="customFile">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="form-label" for="default-06">Select Supervisor </label>
                        <div class="form-control-wrap ">

                            <select class="form-control" id="default-06" required name="supervisor_id">
                                @foreach($supervisorallocation as $supervisorAllocations )
                                    <option value="{{App\User::find($supervisorAllocations->supervisor_id)->id ?? ''}}" >{{App\User::find($supervisorAllocations->supervisor_id)->name ?? ''}}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>
                    @csrf
                    <div class="form-group">
                        <button type="submit" class="btn btn-lg btn-primary btn-submit">Upload Project</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal Tabs -->
