@extends('layouts.dashboard')

@section('content')
    @role('coordinator')
    @include("partials.coordinator_home")
    @endrole

    @role('student')
    @include("partials.student_home")
    @endrole

    @role('supervisor')
    @include("partials.supervisor_home")
    @endrole
@endsection
