<!-- content @s -->
@extends("layouts.dashboard")
@section("content")
    <!-- content @s -->
    <div class="nk-content nk-content-fluid">
        <div class="container-xl wide-xl">
            <div class="nk-content-inner">
                <div class="nk-content-body">
                    <div class="nk-block-head nk-block-head-sm">
                        <div class="nk-block-between">
                            <div class="nk-block-head-content">
                                <h3 class="nk-block-title page-title">Student and Supervisor Allocation Dashboard</h3>
                                <div class="nk-block-des text-soft">
                                    <p>Welcome  {{\Illuminate\Support\Facades\Auth::user()->name }}</p>
                                </div>
                            </div><!-- .nk-block-head-content -->
                            <div class="nk-block-head-content">
                                <div class="toggle-wrap nk-block-tools-toggle">
                                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-more-v"></em></a>
                                    <div class="toggle-expand-content" data-content="pageMenu">
                                        <ul class="nk-block-tools g-3">
                                            <li><a href="#" class="btn btn-white btn-dim btn-outline-primary"><em class="icon ni ni-download-cloud"></em><span>Export</span></a></li>
                                            <li><a href="#" class="btn btn-white btn-dim btn-outline-primary"><em class="icon ni ni-reports"></em><span>Reports</span></a></li>
                                            <li class="nk-block-tools-opt">
                                                <div class="drodown">
                                                    <a href="#" class="dropdown-toggle btn btn-icon btn-primary" data-toggle="modal" data-target="#addAdmin"><em class="icon ni ni-plus"></em></a>

                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div><!-- .nk-block-head-content -->
                        </div><!-- .nk-block-between -->
                    </div><!-- .nk-block-head -->



                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (session('danger'))
                        <div class="alert alert-danger">
                            {{ session('danger') }}
                        </div>
                    @endif
                    <div class="nk-block">
                        <div class="row g-gs">

                            <div class="col-lg-12">
                                <div class="card card-bordered card-full">
                                    <div class="card-inner p-0 border-top">
                                        <div class="nk-tb-list nk-tb-orders">
                                            <div class="nk-tb-item nk-tb-head">
                                                <div class="nk-tb-col nk-tb-orders-type"><span>S/N</span></div>
                                                <div class="nk-tb-col"><span>Student</span></div>
                                                <div class="nk-tb-col tb-col-sm"><span>Student Phone Number</span></div>
                                                <div class="nk-tb-col tb-col-sm"><span>Supervisor</span></div>
                                                <div class="nk-tb-col tb-col-sm"><span>Supervisor Phone Number</span></div>
                                                <div class="nk-tb-col tb-col-xl"><span>Assigned Date</span></div>
                                                <div class="nk-tb-col tb-col-xl"><span>Delete</span></div>

                                            </div><!-- .nk-tb-item -->

                                            @foreach($allocations as $count=>$allocation)
                                                <div class="nk-tb-item">
                                                    <div class="nk-tb-col">
                                                        <span class="tb-lead">{{$count+1}}</span>
                                                    </div>
                                                    <div class="nk-tb-col">
                                                        <span class="tb-lead">{{App\User::find($allocation->student_id)->name}}</span>
                                                    </div>
                                                    @if (empty(App\Allocation::find($allocation->student_id)->phone) || empty(App\Allocation::find($allocation->supervisor_id)->phone))
                                                        <div class="nk-tb-col tb-col-sm">
                                                            <span class="tb-sub">09067342356</span>
                                                        </div>
                                                    @else
                                                        <div class="nk-tb-col tb-col-sm">
                                                            <span class="tb-sub">{{App\Allocation::find($allocation->student_id)->phone}}</span>
                                                        </div>
                                                    @endif
                                                    <div class="nk-tb-col tb-col-sm">
                                                        <span class="tb-sub">{{App\User::find($allocation->supervisor_id)->name}}</span>
                                                    </div>
                                                    @if (empty(App\Allocation::find($allocation->student_id)->phone) || empty(App\Allocation::find($allocation->supervisor_id)->phone))
                                                        <div class="nk-tb-col tb-col-sm">
                                                            <span class="tb-sub">090390042356</span>
                                                        </div>
                                                    @else
                                                        <div class="nk-tb-col tb-col-sm">
                                                            <span class="tb-sub">{{App\Allocation::find($allocation->student_id)->phone}}</span>
                                                        </div>
                                                    @endif


                                                    <div class="nk-tb-col tb-col-xl">
                                                        <span class="tb-sub">{{db_to_human_time($allocation->created_at)}}</span>
                                                    </div>

                                                    <div class="nk-tb-col tb-col-xl">
                                                        <a href="{{route("allocation.delete",$allocation->id)}}"><span class="tb-sub">Delete</span></a>
                                                    </div>

                                                </div><!-- .nk-tb-item -->
                                            @endforeach


                                        </div>
                                    </div><!-- .card-inner -->
                                </div><!-- .card -->
                            </div><!-- .col -->
                        </div><!-- .row -->
                    </div><!-- .nk-block -->
                </div>
            </div>
        </div>
    </div>
    <!-- content @e -->



    <!-- Modal Form -->
    <div class="modal fade" tabindex="-1" id="addAdmin">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Allocate Student to Supervisor</h5>
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <em class="icon ni ni-cross"></em>
                    </a>
                </div>
                <div class="modal-body">
                    <form action="{{route('create.allocation')}}" method="POST" class="form-validate form is-alter" id="form">
                        <div class="form-group">
                            <label class="form-label" for="default-06">Select Student</label>
                            <div class="form-control-wrap ">
                                <div class="form-control-select">
                                    <select class="form-control" id="default-06" required name="student_id">
                                        @foreach($students as $student )
                                        <option value="{{$student->id}}" >{{$student->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="form-label" for="default-06">Select Supervisor </label>
                            <div class="form-control-wrap ">

                                    <select class="form-control" id="default-06" required name="supervisor_id">
                                        @foreach($supervisors as $supervisor )
                                            <option value="{{$supervisor->id}}"  >{{$supervisor->name}}</option>
                                        @endforeach
                                    </select>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Allocation Date</label>
                            <div class="form-control-wrap">
                                <div class="form-icon form-icon-left">
                                    <em class="icon ni ni-calendar"></em>
                                </div>
                                <input type="text" class="form-control date-picker" name="allocation_date" data-date-format="dd-mm-yy">
                            </div>
                            <div class="form-note">Date format <code>yyyy-mm-dd</code></div>
                        </div>

                        @csrf
                        <div class="form-group">
                            <button type="submit" class="btn btn-lg btn-primary btn-submit">Allocate Supervisor to Student</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Tabs -->






@endsection
