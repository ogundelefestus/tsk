<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectTopic extends Model
{
    protected $fillable = [ 'student_id','project_name'];

    public function student()
    {

        return $this->belongsTo(User::class);
    }

    public function supervisor()
    {

        return $this->belongsTo(User::class);
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

//    public function projecttopic(){
//        return $this->belongsTo(User::class);
//    }
}
