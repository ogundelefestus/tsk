<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    /*Model****Employee*/
    protected $table="students";

    public $timestamps = true;

    public function allocations(){
        return $this->hasMany(Allocation::class);
    }
}
