<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supervisor extends Model
{

    protected $fillable = [
        'phone','user_id',
    ];

    public function allocation(){
        return $this->hasMany(Allocation::class);
    }
}
