<?php
//DB TIME TO HUMAN READABLE TIME HELPER
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

if (! function_exists('db_to_human_time')) {
    function db_to_human_time($time) {
        $dt=Carbon::createFromFormat('Y-m-d H:i:s', $time);
        return $dt->diffForHumans();
    }
}

if (! function_exists('get_picture_letter')) {
    function get_picture_letter($name) {
        $split=explode(" ",$name);
        $initials="";
        foreach ($split as $alphabet)
        {
            $initials.=strtoupper($alphabet[0]);
        }
        return $initials;
    }
}

if (! function_exists('get_email_picture_letter')) {
    function get_email_picture_letter($name='') {
        if(strlen($name)>=2){
            $name=$name[0].$name[1];
        }
        return strtoupper($name);
    }
}


//CHANGE FIRST LETTER TO UPPERCASE
if (! function_exists('uppercase')) {
    function uppercase($string) {
        return Str::ucfirst($string);
    }
}

//GET DOMAIN NAME OF EMAIL
if (! function_exists('get_domain_email')) {
    function get_domain_email($email) {
        $email=explode("@",$email);
        return $email[1];
    }
}

