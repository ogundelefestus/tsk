<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Allocation extends Model
{
    protected $fillable = ['allocation_date', 'student_id', 'supervisor_id','user_id'];

    public function student()
    {

        return $this->belongsTo(User::class);
    }

    public function supervisor()
    {

        return $this->belongsTo(User::class);
    }
}
