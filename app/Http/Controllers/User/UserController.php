<?php

namespace App\Http\Controllers\User;


use App\Http\Controllers\Controller;
use App\Http\Requests\CoordinatorRequest;
use App\Http\Requests\StudentRequest;
use App\Http\Requests\SupervisorRequest;
use App\Supervisor;
use App\User;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{



    /** Supervisor **/
    public function showSupervisor(){
        $supervisor=Supervisor::get();
        return view('user.supervisor',compact('supervisor'));
    }


    public function createStudent(StudentRequest $request)
    {

        $data=$request->all();
        $data['password']= Hash::make($data['password']);

        $user= User::create($data);

        $user->assignRole(['name' => 'Student']);

        $phone=$request->post('phone');
        $matric_number=$request->post('matric_number');

        $student = new Student;
        $student->phone = $phone;
        $student->matric_number = $matric_number;
        $student->user_id = $user->id;


        if ($student->save()) {

            return back()->with('status', 'Student Created Successfully');

        } else {
            return back()->with('danger','Student not Created');

        }




    }

    public function deleteSupervisor(User $user){

        if($user->delete()) {
            return back()->with('status', 'Supervisor Deleted Successfully');
        }
        return back()->with('danger','Supervisor Not Deleted');

    }



    /** Student **/
    public function showStudent(){
        $student=Student::get();
        return view('user.student',compact('student'));
    }



    public function createSupervisor(SupervisorRequest $request)
    {

        $data=$request->all();
        $data['password']= Hash::make($data['password']);

        $user= User::create($data);

        $user->assignRole(['name' => 'Supervisor']);
        $phone=$request->post('phone');


        $supervisor = new Supervisor;
        $supervisor ->phone = $phone;
        $supervisor->user_id = $user->id;


        if ($supervisor->save()) {

            return back()->with('status', 'User Created Successfully');

        } else {
            return back()->withErrorMessage('Client not Created');

                }




        }


    public function deleteStudent(User $user){

        if($user->delete()){
            return back()->with('status','Student Deleted Successfully');
        }
        return back()->with('danger','Student Not Deleted');

    }



    /** Coordinator **/
    public function showCoordinator(){
        $coordinator=User::role('coordinator')->get();

        return view('user.coordinator',compact('coordinator'));
    }

    public function createCoordinator(CoordinatorRequest $request){
        $data=$request->all();
        $data['password']= Hash::make($data['password']);
        $user= User::create($data);
        $user->assignRole(['coordinator']);
        if($user->save()){
            return back()->with('status','Project Coordinator Created Successfully');
        }
        return back()->with('danger',' Project Coordinator Not Created');

    }


    public function deleteCoordinator(User $user){

        if($user->delete()){
            return back()->with('status','Project Coordinator Successfully');
        }
        return back()->with('danger','Project Coordinator Not Deleted');

    }

    public function editAdmin(Request $request,User $user){
        $request->validate([
            "name"=>"required",
            "email"=>"required",
            "salary"=>"nullable"
        ]);
        //Checking if the email inputed is already in the db
        $email=User::where('email','=',$request->post("email"))->first();

        if($email!=NULL && $email->id!=$user->id){

            return back()->with('danger','Email Already Exist');
        }

        if(!empty($request->post("password"))){
            $user->email=$request->post("email");
            $user->name=$request->post("name");
            $user->password=bcrypt($request->post("password"));

            if($user->save()){
                return back()->with('status','User Updated Successfully');
            }

            return back()->with('danger','User Not Updated');
        }

        if(!empty($request->post("salary"))){
            $user->email=$request->post("email");
            $user->name=$request->post("name");
            $user->salary=$request->post("salary");

            if($user->save()){
                return back()->with('status','User Updated Successfully');
            }

            return back()->with('danger','User Not Updated');
        }

        $user->email=$request->post("email");
        $user->name=$request->post("name");
        if($user->save()){

            return back()->with('status','User Updated Successfully');
        }
        return back()->with('danger','User Not Updated ');



    }



}


