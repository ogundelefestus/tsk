<?php

namespace App\Http\Controllers\User;

use App\User;
use Illuminate\Http\Request;
use App\Upload;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UploadController extends Controller
{
public function projectUpload(Request $request)
{

    $request->validate([

        'file' => 'required|mimes:pdf,docx,doc,txt'

    ]);


    $upload = new Upload();
    $path = $request->file('file')->store('public');
    $upload->file = $path;
    $path = storage_path('app') . '\\' . $path;
    $upload->supervisor_id = $request->post('supervisor_id');
    $upload->student_id = auth()->id();

    if ($upload->save()) {
        return back()->with('status', 'Project Submitted To Supervisor Successfully');
    } else {
        return back()->with('danger', 'Upload not Submitted');
    }
}


public function showSupervisorUpload(){

    $id = Auth::user()->id;
    $upload= Upload::where('supervisor_id','=', $id)->get();

   // dd($upload);

    return view('user.show_upload', compact('upload'));



}

}
