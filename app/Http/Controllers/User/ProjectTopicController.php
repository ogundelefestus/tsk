<?php

namespace App\Http\Controllers\User;

use App\Allocation;
use App\Http\Controllers\Controller;

use App\ProjectTopic;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;


class ProjectTopicController extends Controller
{
    public function showProjectTopic()
    {
        $id = Auth::user()->id;
        $students=Allocation::where('supervisor_id','=', $id)->get();
        $data=ProjectTopic::get();

        return view('user.projecttopic',compact('students','data'));
    }


    public function showAdminProjectTopic()
    {
        $data=ProjectTopic::all();

        return view('user.adminproject',compact('data'));
    }


    public function createProjectTopic(Request $request){

        $request->validate([
            'project_name' => 'required',
        ]);

        $data = new ProjectTopic;
        $data->project_name = $request->post('project_name');
        $data->student_id = $request->post('student_id');

        if ($data->save()) {
            $student = User::role('student')->get();
            return back()->with('status', 'Project Topic Assigned Successfully');
        }
    }


    public function deleteProjectTopic($id){

        $projectTopic = ProjectTopic::where('id', $id)->firstorfail()->delete();
        return back()->with('danger', 'Project Topic Deleted Successfully');


    }
}
