<?php

namespace App\Http\Controllers\User;

use App\Allocation;
use App\Http\Controllers\Controller;
use App\Student;
use App\User;
use Carbon\Carbon;
use DB;
use Auth;
use App\Task;
use App\Supervisor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Spatie\Permission\Models\Role;

class TaskController extends Controller
{

    public function showTask(){
        $id = Auth::user()->id;
        $studentallocation= Allocation::where('supervisor_id','=', $id)->get();
        $studentTask=Task::get();
       // dd($studentTask);
        return view('user.task', compact('studentallocation','studentTask'));

    }

    public function createTask(Request $request)
    {

        $request->validate([
            'task_description' => 'required',
        ]);

        $data = new Task;
        $data->task_description=$request->post('task_description');
        $data->student_id = $request->post('student_id');

        if ($data->save()) {
            $student = User::role('student')->get();
            $supervisor = User::role('supervisor')->get();
            return back()->with('status', 'Task Created Successfully');
        }


    }


    /** Allocation **/
    public function showAllocation()
    {

        $students = User::role("student")->get();
        $supervisors = User::role("supervisor")->get();
        $allocations = Allocation::all();

        return view('user.allocation', compact('students', 'supervisors', 'allocations'));
    }


    public function createAllocation(Request $request)
    {

        $request->validate([
            'allocation_date' => 'required',

        ]);

        $data = new Allocation;
        $data['allocation_date'] = Carbon::now();
        //$data->start_date=Carbon::createFromFormat('d/m/Y', $request->post('allocation_date'))->toDateString();
        $data->student_id = $request->post('student_id');
        $data->supervisor_id = $request->post('supervisor_id');
        if ($data->save()) {
            $student = User::role('student')->get();
            $supervisor = User::role('supervisor')->get();
            return back()->with('status', 'Allocation Created Successfully');
        }


    }

    public function deleteAllocation(User $user){

        if($user->delete()){
            return back()->with('status','Allocation Deleted Successfully');
        }
        return back()->with('danger','Allocation Not Deleted');


    }



}
