<?php

namespace App\Http\Controllers;

use App\Allocation;
use App\ProjectTopic;
use App\User;
use Auth;
Use DB;
Use App\Task;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $id = Auth::user()->id;
        $userCount = User::count();
        $supervisorCount=User::role("supervisor")->count();
        $studentCount=User::role("student")->count();
        $coordinatorCount=User::role("coordinator")->count();
        $supervisorallocation= Allocation::where('student_id','=', $id)->get();
        $user=User::all();

        if(User::role("supervisor")){

           $id = Auth::user()->id;
          $allocations= Allocation::where('supervisor_id','=', $id)->get();
          $allocationCount= Allocation::with('student_id',$id)->count();

        }


        if(User::role("student")){

            $id = Auth::user()->id;
            $allocation= Allocation::where('student_id','=', $id)->first();
            $collection= ProjectTopic::where('student_id','=', $id)->get();
            $task= Task::where('student_id','=', $id)->get();

          //  dd($task);

            //dd('baab');

        }




        return view('home',compact('userCount','supervisorallocation','supervisorCount','studentCount','coordinatorCount','user','allocation','collection','allocations','allocationCount','task'));
    }
}
