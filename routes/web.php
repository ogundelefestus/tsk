<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect("login");
});
Auth::routes(['register' => false]);

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');


Route::group([
    'middleware' => ['auth', 'role:coordinator'],
    'namespace'=>'User',
    'prefix'=>'user',
], function() {

    //System Setting

    Route::get('/setting', 'SettingController@showSettings')->name('show.setting');
    Route::post('/create/setting', 'SettingController@postSettings')->name('post.setting');

//User Management Coordinator
    Route::get('user', 'UserController@showCoordinator')->name('show.user');
    Route::post('create/user', 'UserController@createCoordinator')->name('create.user');
    Route::get('/delete_coordinator/{user?}', 'UserController@deleteCoordinator')->name('coordinator.delete');
    Route::post('/edit_admin/{user?}', 'UserController@editAdmin')->name('admin.edit');


//User Management Student
    Route::get('student', 'UserController@showStudent')->name('show.student');
    Route::post('create/student', 'UserController@createStudent')->name('create.student');
    Route::get('/delete_student/{user}', 'UserController@deleteStudent')->name('student.delete');

//User Management Supervisor
    Route::get('supervisor', 'UserController@showSupervisor')->name('show.supervisor');
    Route::post('create/supervisor', 'UserController@createSupervisor')->name('create.supervisor');
    Route::get('/delete_supervisor/{user}', 'UserController@deleteSupervisor')->name('supervisor.delete');


   //Task Management
    Route::get('/show_allocation', 'TaskController@showAllocation')->name('show.allocation');
    Route::post('set/allocation', 'TaskController@createAllocation')->name('create.allocation');
    Route::get('/delete_allocation/{user}', 'TaskController@deleteAllocation')->name('allocation.delete');

    //Project Management
    Route::get('/admin_show_project', 'ProjectTopicController@showAdminProjectTopic')->name('show.adminproject');





});






Route::group([
    'middleware' => ['auth', 'role:supervisor'],
    'namespace'=>'User',
    'prefix'=>'user',
], function() {


    //Project Management
    Route::get('/show_project', 'ProjectTopicController@showProjectTopic')->name('show.project');
    Route::post('/create_project', 'ProjectTopicController@createProjectTopic')->name('create.project');
    Route::get('/delete_project/{id}', 'ProjectTopicController@deleteProjectTopic')->name('projecttopic.delete');

    //Real Task

    Route::get('/task', 'TaskController@showTask')->name('show.task');
    Route::post('create_task', 'TaskController@createTask')->name('create.task');


    //Show Upload Management
    Route::get('/show_upload','UploadController@showSupervisorUpload')->name('show.upload');


});



Route::group([
    'middleware' => ['auth', 'role:student'],
    'namespace'=>'User',
    'prefix'=>'user',
], function() {


    //Upload Management
    Route::post('/upload','UploadController@projectUpload')->name('create.upload');


});

